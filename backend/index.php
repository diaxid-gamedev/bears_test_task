<?php
require_once "game.php";
require_once "user.php";
header("Access-Control-Allow-Origin: *");

$recievedRequest = file_get_contents('php://input');
if (isset($recievedRequest) && !empty($recievedRequest)) {
    $request = json_decode($recievedRequest);
}
if (isset($request->vk_id)) {
    $vk_id = $request->vk_id;
} else {
    $vk_id = 1;
}
$game = new Game($vk_id);
$player = new User($vk_id);
$winners = $game->getWinners();
$data = [
    'winners' => $winners
    //[
    //     [
    //         'id' => 1,
    //         'img' => '',
    //         'name' => 'Оксана Киркорова',
    //         'isJackpot' => false,
    //         'prize' => 999,
    //         'time' => 24
    //     ],
        
    //]
];
switch ($request->state) {
    case 'spin':
        $data += $game->play();
        break;
    default:
        $data += [
            'balance' => $player->getBalance(),
            'rotation' => 0,
            'prize' => 0,
            'sector' => 0,
            'isJackpot' => false,
            'jackpotAmount' => $game->getJackpot()
        ];
        break;
}
$data = json_encode($data);
print_r($data);
?>