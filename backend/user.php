<?php
require_once "config.php";
require_once "db.php";
class User
{
    private $id;
    private $vkId;
    private $balance;
    public function __construct($vkId)
    {
        $this->vkId = $vkId;
        $this->getUserFromDb();
    }
    function getUserFromDb()
    {
        $result = queryDB("SELECT `id`,`balance` FROM `users` WHERE `users`.`vk_id` = {$this->vkId};");
        $result = mysqli_fetch_array($result);
        if ($result["id"] == "") {
            $this->addUser();
            $this->getUserFromDb();
        } else {
            $this->balance = $result["balance"];
            $this->id = $result["id"];
        }
    }
    private function addUser()
    {
        queryDB("INSERT INTO `users` (`vk_id`, `balance`, `created`, `updated`) VALUES ({$this->vkId}, 100, current_timestamp(), current_timestamp());");
        $this->balance = 100;
    }

    public function incrementCoins($count)
    {
        $this->balance += $count;
        updateDataInDb($this->id, 'balance', $this->balance);
    }

    public function decrementCoins($count)
    {
        $this->balance -= $count;
        updateDataInDb($this->id, 'balance', $this->balance);
    }
    public function getBalance()
    {
        return $this->balance;
    }
    public function getId()
    {
        return $this->id;
    }
}
?>