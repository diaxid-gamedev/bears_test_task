<?php
require_once "db.php";
require_once "user.php";

class Game
{
    private $sector;
    private $rotation;
    private $jackpotAmount;
    private $winners;
    private $player;
    private $price = 100;
    public function __construct($userId)
    {
        $this->getJackpot();
        $this->player = new User($userId);
    }
    public function getJackpot()
    {
        $result = queryDB("SELECT `balance` FROM `users` WHERE `users`.`id` = 777;");
        $result = mysqli_fetch_array($result);
        $this->jackpotAmount = $result["balance"];
        return $this->jackpotAmount;
    }
    private function incrementJackpot($count)
    {
        $this->jackpotAmount += $count;
        updateDataInDb(777, 'balance', $this->jackpotAmount);
    }

    private function decrementJackpot($count)
    {
        $this->jackpotAmount -= $count;
        updateDataInDb(777, 'balance', $this->jackpotAmount);
    }
    private function getRotationDegrees($sector)
    {
        $deg = $sector * 45 + rand(5, 48);
        return $deg;
    }
    private function isJackpot($prize)
    {
        if ($prize == "Jackpot") {
            return true;
        } else {
            return false;
        }
    }
    private function getWinAmount($sector)
    {
        switch ($sector) {
            case 0:
                return 750;
            case 1:
                return 200;
            case 2:
                return 150;
            case 3:
                return 100;
            case 4:
                return 10;
            case 5:
                return 400;
            case 6:
                return 250;
            case 7:
                return "Jackpot";
        }
    }
    public function play()
    {
        $this->player->decrementCoins($this->price);
        $this->incrementJackpot($this->price);
        $random = rand(0, 7);
        $deg = $this->getRotationDegrees($random);
        $prize = $this->getWinAmount($random);
        $isJackpot = $this->isJackpot($prize);
        if ($isJackpot) {
            $prize = $this->getJackpot();
            $this->decrementJackpot($prize);
        }
        $this->player->incrementCoins($prize);
        $gameData = [
            'sector' => $random,
            'rotation' => $deg,
            'prize' => $prize,
            'balance' => $this->player->getBalance(),
            'isJackpot' => $isJackpot,
            'jackpotAmount' => $this->jackpotAmount
        ];
        $this->addWinner($this->player->getId(), $isJackpot, $prize);
        return $gameData;
    }

    private function addWinner(int $id, bool $jackpot, $prize)
    {
        if ($jackpot == "") {
            $jackpot = "0";
        }
        $result = queryDB("INSERT INTO `winners` (`winner_id`, `jackpot`, `amount`, `date_time`) VALUES ({$id}, {$jackpot}, {$prize}, current_timestamp());");
    }
    public function getWinners()
    {
        $winners = [];
        $result = queryDB("SELECT * FROM `winners` ORDER BY `winners`.`id` DESC LIMIT 4;");
        $result = mysqli_fetch_all($result, MYSQLI_ASSOC);

        foreach ($result as $element){
            $winner["id"] = (int) $element["winner_id"];
            $winner["img"] = '';
            $winner["name"] = '';
            $winner["prize"] = (int) $element["amount"];
            $winner["isJackpot"] = (bool) $element["jackpot"];
            $winner["time"] = $element["date_time"];
            $winners[] = $winner;
        }

        return $winners;
    }
}



?>