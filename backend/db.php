<?php
require_once "config.php";
function db(){
    global $config;
    $db = mysqli_connect("{$config['db']['host']}", "{$config['db']['user']}", "{$config['db']['password']}", "{$config['db']['name']}");
    if ($db == false) {
        echo ("Не удалось подключиться к базе!<br>" . mysqli_connect_error());
    }
    return $db;
}


function updateDataInDb($userId, string $column, $value)
{
    $db = db();
    $sql = "UPDATE `users` SET `{$column}` = {$value} WHERE `users`.`id` = {$userId};";
    $result = mysqli_query($db, $sql);
    if ($result == false) {
        print("Произошла ошибка при выполнении запроса: ".$sql);
        print(mysqli_error($db));
    }
}
function queryDB($sql){
    $db = db();
    $result = mysqli_query($db, $sql);
    if ($result == false) {
        print("Произошла ошибка при выполнении запроса: ".$sql);
        print(mysqli_error($db));
    } else {
        return $result;
    }
}

?>