import React, { useState, useEffect } from 'react'
import Wheel from "./components/Wheel"
import Balance from "./components/Balance"
import Jackpot from "./components/Jackpot"
import Navbar from "./components/Navbar"
import SpinButton from "./components/SpinButton"
import WinnersList from "./components/WinnersList"
import Winning from "./components/Winning"
import debugMessage from './components/debug'

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            winners: [
                {
                    "id": 0,
                    "img": "",
                    "name": "NoData",
                    "isJackpot": false,
                    "prize": 0,
                    "time": 0
                }
            ],
            balance: 0,
            rotation: 0,
            prize: 0,
            sector: 0,
            isJackpot: false,
            jackpotAmount: 0,
            state: 'start',
            renderPopUp: false,
        }
        this.getGameData = this.getGameData.bind(this)
        this.spin = this.spin.bind(this)
        this.closePopup = this.closePopup.bind(this)
    }

    render() {
        debugMessage("app props ", this.props)
        debugMessage("app state ", this.state)
        if (this.state.winners[0].name === "NoData")
            this.getGameData(this.props.id, this.state.state)
        return (
            <div>
                <header>
                    <Navbar />
                </header>
                <div className="container">

                    <main>
                        <h1 className="title">wheel of fortune</h1>

                        <div className="gameplay">
                            <Wheel rotation={this.state.rotation} />
                            <div className="controls">
                                <Jackpot jackpotAmount={this.state.jackpotAmount} />
                                <Balance balance={this.state.balance} />
                                <div className="border">
                                    <SpinButton spin={this.spin} state={this.state.state} />
                                </div>
                            </div>
                        </div>
                        <div className="liderboard">
                            <h2 className="title">winners</h2>
                            <WinnersList winners={this.state.winners} />
                        </div>

                    </main >
                </div >
                <Winning rendering={this.state.renderPopUp} prize={this.state.prize} closePopup={this.closePopup} />
            </div>
        )
    }

    getGameData(id, state) {
        fetch("http://bearstesttask/backend/", {
            method: 'POST',
            header: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: JSON.stringify({ vk_id: id, state: state })
        })
            .then(response => response.json())
            .then(response => {
                debugMessage("app response", response)
                this.setState({
                    winners: response.winners,
                    balance: response.balance,
                    rotation: response.rotation,
                    prize: response.prize,
                    sector: response.sector,
                    isJackpot: response.isJackpot,
                    jackpotAmount: response.jackpotAmount
                })
            })

    }
    spin(id) {
        this.getGameData(id, 'spin')
        this.setState({ state: 'spin' })
        setTimeout(function () { this.setState({ renderPopUp: true }) }.bind(this), 5000)
    }
    closePopup() {
        this.setState({ renderPopUp: false })
        this.setState({ rotation: 0 })
        this.setState({ state: 'start' })
    }

}

export default App;
