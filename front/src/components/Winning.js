
import React from "react"

class Winning extends React.Component {
    render() {
        if (this.props.rendering)
            return (
                <div className="pop-up">
                    <span className="mt-4">you win!</span>
                    <span className="mt-4">{this.props.prize} <img src="assets/images/coins_l.png" alt="" /></span>
                    <div className="border mt-4">
                        <div className="button action great" onClick={() => this.props.closePopup()}>
                            <span>great</span>
                        </div>
                    </div>
                </div>
            )
        else return <div></div>
    }
}
export default Winning




