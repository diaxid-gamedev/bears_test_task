import React from "react"
import debugMessage from "./debug"

class Balance extends React.Component {
    render() {
        debugMessage("balance props",this.props)
        return (
            <div className="button info">
                <h3>balance</h3>
                <h3>{this.props.balance}</h3>
            </div>
        )
    }
}
export default Balance