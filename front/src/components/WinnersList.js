import Winner from "./Winner"
import React from "react"
import debugMessage from "./debug"

class WinnersList extends React.Component {
    render() {
        debugMessage("winnerslist props",this.props)
        if (this.props.winners.length > 0)
            return (
                <ul className="winners">
                    {this.props.winners.map((element) => (
                        <Winner key={element.id} winner={element} />
                    ))}
                </ul>
            )
        else
            return (<h2>Победителей еще нет</h2>)
    }
}
export default WinnersList