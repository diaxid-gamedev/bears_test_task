import React from "react"
import closeIcon from "../images/Icon_close.png"
import moreIcon from "../images/Icon_more.png"


class Navbar extends React.Component {
    render() {
        return (
            <nav>
                <div id="more"><img src={closeIcon} alt="" /></div>
                <div id="devider"></div>
                <div className="close"><img src={moreIcon} alt="" /></div>
            </nav>
        )
    }
}
export default Navbar