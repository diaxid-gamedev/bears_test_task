import React from "react"
import coins from '../images/coins_s.png'
import avatar from '../images/Avatar.png'
import debugMessage from "./debug"

class Winner extends React.Component {
    
    render() {
        debugMessage("winner props",this.props)
        return (
            <li>
                <img src={avatar} className="avatar" alt="" />
                <span className="user">{this.props.winner.name}</span>

                {this.props.winner.isJackpot ? <span className="jackpot">jackpot</span> : <span className="prize">{this.props.winner.prize} <img src={coins} alt="" /></span>}

                <span className="time">{Date()-this.props.winner.time} c.</span>
            </li>
        )
    }
}
export default Winner