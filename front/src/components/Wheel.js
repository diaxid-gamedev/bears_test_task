import wheel from "../images/wheel1.png"
import pointer from "../images/wheel-pointer1.png"
import React from "react"
import debugMessage from "./debug"

// let style = {{@keyframes spin {0 % {transform: rotate(0deg)}10 % {transform: rotate(-4700deg)}100 % { transform: rotate(${this.props.rotation}deg)}}.spin {animation: spin 2s ease - out 0s forwards;}}}
class Wheel extends React.Component {
    render() {

        debugMessage("wheel props", this.props)
        if (this.props.rotation !== 0)
            return (
                <div className="wheel-box">
                    <img id="pointer" src={pointer} alt="" />
                    <img className="spin" id="wheel" src={wheel} alt="" style={{transform: `rotate(${this.props.rotation}deg)`}} />
                </div>
            )
        else
            return (
                <div className="wheel-box">
                    <img id="pointer" src={pointer} alt="" />
                    <img id="wheel" src={wheel} alt="" />
                </div>
            )
    }
}
export default Wheel



