
import React from "react"

class SpinButton extends React.Component {
    render() {
        if (this.props.state === 'spin')
            return (<div className="button action">
                <h3>spin</h3>
                <h3>wheel</h3>
            </div>)
        else
            return (
                <div className="button action" onClick={() => this.props.spin(1)}>
                    <h3>spin</h3>
                    <h3>wheel</h3>
                </div>
            )
    }
}
export default SpinButton