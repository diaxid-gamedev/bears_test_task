import React from "react"
import debugMessage from "./debug"

class Jackpot extends React.Component {
    
    render() {
        debugMessage("jackpot props",this.props)
        return (
            <div className="button info">
                <h3>jackpot</h3>
                <h3>{this.props.jackpotAmount}</h3>
            </div>
        )
    }
}
export default Jackpot