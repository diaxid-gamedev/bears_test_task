
function debugMessage(header,message) {
    const debug = true

    if (debug) console.log(Date.now()/1000, header, message)
}
export default debugMessage